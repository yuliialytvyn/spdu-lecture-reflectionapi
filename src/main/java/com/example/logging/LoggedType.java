package com.example.logging;

public interface LoggedType {

    @Logged
    int calculate();

    @Logged
    int getVal1();

}
