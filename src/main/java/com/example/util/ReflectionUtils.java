package com.example.util;

import java.lang.reflect.Modifier;

/**
 * Source of example: http://www.javenue.info/post/84
 */

public class ReflectionUtils {
    public static String getModifiers(int m) {
        String modifiers = "";
        if (Modifier.isPublic(m)) modifiers += "public ";
        if (Modifier.isProtected(m)) modifiers += "protected ";
        if (Modifier.isPrivate(m)) modifiers += "private ";
        if (Modifier.isStatic(m)) modifiers += "static ";
        if (Modifier.isAbstract(m)) modifiers += "abstract ";
        return modifiers;
    }

    public static String getType(Class clazz) {
        String type = clazz.isArray()
                ? clazz.getComponentType().getSimpleName()
                : clazz.getSimpleName();
        if (clazz.isArray()) type += "[]";
        return type;
    }

    public static String getParameters(Class[] params) {
        StringBuilder p = new StringBuilder();
        for (int i = 0, size = params.length; i < size; i++) {
            if (i > 0) {
                p.append(", ");
            }
            p.append(getType(params[i])).append(" param").append(i);
        }
        return p.toString();
    }
}
