package com.example.util;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;

public class LogUtil {
    public static void log(Class<?> clazz, Method method, Object[] args) {
        System.out.print("\n" + clazz.getSimpleName() + ": "
                + "Invoke method : [ \u001B[33m" + method.getName() + "()\u001B[0m ]"
                + (Objects.isNull(args) ? "" : " with parameter : \u001B[31m" + Arrays.toString(args)) + "\u001B[0m\n\t");
    }

    public static void logAnnotationNotFound(String title, String name, Class annotationClazz) {
        System.out.print(title + "'" + name + "' has no " + annotationClazz.getSimpleName() + " annotation.\n\t");
        System.out.print("Just call real method. \n\t");
    }

    public static void log(String methodName, Object meow) {
        System.out.println("\nProxied " + "\u001B[33m" + methodName + "() "
                + "\u001B[0m" + "is called with parameter " + "\u001B[31m" + meow + " \u001B[0m");
    }
}
