package com.example.proxy;

import com.example.annotation.AnnotationCat;
import com.example.annotation.InteractiveAnimal;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

public class DynamicProxyDemo {

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        InteractiveAnimal animalProxy = getAnimalProxyInstance();
//        InteractiveAnimal animalProxy = createAnimalProxyInstance();

        System.out.println(animalProxy.sayingSmth("meow"));
        System.out.println(animalProxy.sayingSmth("meow meow"));
        System.out.println(animalProxy.sayingSmth("MEOW"));

        System.out.println(animalProxy.letDownWaterGlass(true));

    }

    private static InteractiveAnimal getAnimalProxyInstance() {
        return (InteractiveAnimal) Proxy.newProxyInstance(
                InteractiveAnimal.class.getClassLoader(),
                new Class[] {InteractiveAnimal.class},
                new DynamicProxyHandler(new AnnotationCat()));
    }

    private static InteractiveAnimal createAnimalProxyInstance() throws InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Class<?> proxyClass = Proxy.getProxyClass(AnnotationCat.class.getClassLoader(), AnnotationCat.class.getInterfaces());
        DynamicProxyHandler handler = new DynamicProxyHandler(new AnnotationCat());

        return (InteractiveAnimal) proxyClass
                .getConstructor(InvocationHandler.class)
                .newInstance(handler);
    }
}
