package com.example.proxy;

import com.example.annotation.InteractiveAnimal;

import static com.example.util.LogUtil.log;

public class SimpleAnimalProxy {
    private InteractiveAnimal obj;

    public SimpleAnimalProxy(InteractiveAnimal obj) {
        this.obj = obj;
    }

    public String sayingSmth(String meow) {
        log("sayingSmth", meow);
        return obj.sayingSmth(meow);
    }

    public String letDownWaterGlass(boolean justDoIt) {
        log("letDownWaterGlass", justDoIt);
        if (justDoIt) {
            System.out.println("Noooo. Action forbidden");
            return "Ohh ok...";
        }
        return obj.letDownWaterGlass(justDoIt);
    }
}