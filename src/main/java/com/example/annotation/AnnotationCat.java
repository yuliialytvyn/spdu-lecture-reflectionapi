package com.example.annotation;

@AnimalTranslator(species = "Cat")
public class AnnotationCat implements InteractiveAnimal {

    @AnimalTranslator(language = "ru")
    @Override
    public String sayingSmth(String meow) {
        return meow;
    }

    @Override
    public String letDownWaterGlass(Boolean justDoIt) {
        return justDoIt ? "Crack!!!" : "just waiting human to go away...";
    }

}
