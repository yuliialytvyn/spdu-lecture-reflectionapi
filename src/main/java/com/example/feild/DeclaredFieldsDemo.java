package com.example.feild;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public class DeclaredFieldsDemo {

    public static void main(String[] args) {
        for (Field declaredField : ArrayList.class.getDeclaredFields()) {
            if (!declaredField.getName().equals("size")) continue;
            log(declaredField, new ArrayList());
            declaredField.setAccessible(declaredField.getName().equals("size") ? true : false);
            log(declaredField, new ArrayList());
        }
    }

    private static void log(Field declaredField, Object instance) {
        System.out.println("filed '" + declaredField.getName()
                + "' is accessible: "
                + declaredField.canAccess(Modifier.isStatic(declaredField.getModifiers()) ? null : instance));
    }
}
