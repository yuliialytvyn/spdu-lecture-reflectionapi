package com.example.feild;

import java.lang.reflect.Field;

public class SyntheticFieldDemo {

    public static void main(String... var) {
        Field[] fields = User.NestedClass.class.getDeclaredFields();
        System.out.printf("%n[INFO] -- This class should contain only one field: %s", fields.length);

        for (Field f : fields) {
            System.out.printf("%n[INFO] -- Field: %s, isSynthetic: %s\n", f.getName(), f.isSynthetic());
        }
    }
}
